(define-module (language python with)
  #:use-module (language python try)
  #:use-module (language python exceptions)
  #:use-module (language python persist)
  #:use-module (oop pf-objects)
  #:use-module (oop goops)
  
  #:export (with enter exit))

(define-syntax-rule (aif it p x y) (let ((it p)) (if it x y)))

(define-method (enter o)
  (raise TypeError "no __enter__ member"))  
(define-method (enter (o <p>))
  (aif it (ref o '__enter__)
       (it)
       (next-method)))

(define-method (exit . l)
  (pk l)  
  (raise TypeError "no __exit__ member"))
(define-method (exit (o <p>) . l)
  (aif it (ref o '__exit__)
       (apply it l)
       (next-method)))

(name-object enter)
(name-object exit)

(define-syntax with
  (syntax-rules ()
    ((_ () . code)
     (begin . code))
    ((_ (x . l) . code)
     (with0 x (with l . code)))))

(define-syntax with0
  (syntax-rules ()
    ((_ (id exp) . code)
     (let ((type   None)
	   (value  None)
	   (trace  None))
       (let ((vexit   (lambda l (apply exit l)))
             (venter  (lambda () (enter exp)))
	     (id      'id))
         (catch #t
           (lambda ()
             (try
              (lambda ()
                (set! id (venter))
                . code)
              (#:except #t =>
                (lambda (tag l)
                  (set! type  (if (pyclass? tag)
                                  tag
                                  (aif it (ref tag '__class__)
                                       it
                                       tag)))		      
                  (set! value
                    (aif it (ref tag 'value)
                         it
                         (if (pair? l)
                             (car l)
                             None)))
                  (raise tag l)))
              #:finally
              (lambda ()
                (vexit id type value trace))))
           (lambda x (apply throw x))))))
       ((_ (exp) . code)
        (with0 (id exp) . code))))

  
