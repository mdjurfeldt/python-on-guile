(define-module (language python guilemod)
  #:use-module (system base message)
  #:use-module (system foreign)
  #:use-module (ice-9 format)
  #:use-module (language python adress)
  #:use-module ((oop dict) #:select
                (procedure-properties- procedure-property-
                                      set-procedure-property!-))
  #:export (%add-to-warn-list %dont-warn-list %eval-no-warn))

(define-syntax-rule (defineu f a x)
  (begin
    (define f
      (catch #t
        (lambda () x)        
        (lambda z
          (let ((message (format #f "could not define ~a" 'f)))
            (warn message)
            (lambda z (error message))))))))

(define-syntax-rule (aif it p . l) (let ((it p)) (if it . l)))

(define %eval-no-warn (make-hash-table))

(define-syntax-rule (mk-commands path mod-C define-C define-exp-C define-set-C)
  (begin
    (define mod-C (resolve-module 'path))
    (define-syntax-rule (define-C f val)
      (begin
        (define f val)
        (module-define! mod-C 'f f)))

    (define-syntax-rule (define-exp-C f val)
      (let ()
        (define f val)
        (module-define! mod-C 'f val)
        (module-export! mod-C (list 'f))))

    (define-syntax-rule (define-set-C f val)
      (module-set! mod-C 'f (let ((x val)) x)))))

(define %dont-warn-list (make-fluid '()))
(define %add-to-warn-list
  (lambda (sym)
    (fluid-set! %dont-warn-list
                (cons sym
                      (fluid-ref
                       %dont-warn-list)))))

(mk-commands (system repl debug)   mod-R  define-R  define-exp-R  define-set-R)
(mk-commands (system base compile) mod-C  define-C  define-exp-C  define-set-C)
(mk-commands (system vm   program) mod-C2 define-C2 define-exp-C2 define-set-C2)
(mk-commands (system base message) mod-M  define-M  define-exp-M  define-set-M)
(mk-commands (guile)               mod-G  define-G  define-exp-G  define-set-G)
(define-syntax-rule (C x) (@@ (system base compile) x))
(define-syntax-rule (M x) (@@ (system base message) x))

(define *do-extension-dispatch* #t)
(define *extension-dispatches*  '((("py"  "python") . python)
                                  (("pl"  "prolog") . prolog)
                                  (("plb" "prolog-boot") . prolog-boot)))

(define-exp-C %current-file% (make-fluid '(guile)))

(define-C default-language
  (lambda (file)
    (define default ((C current-language)))
    (if *do-extension-dispatch*
        (let ((ext (car (reverse (string-split file #\.)))))
          (let lp ((l *extension-dispatches*))
            (if (pair? l)
                (if (member ext (caar l))
                    (let ((r (cdar l)))
                      (if ((C language?) default)
                          (if (eq? ((C language-name) default) r)
                              default
                              r)
                          r))
                    (lp (cdr l)))
                default)))
        default)))


(define-exp-C %in-compile (make-fluid #f))
(define-exp-C %in-file    (make-fluid #f))

(define-set-C compile-file
  (lambda* (file #:key
                 (output-file      #f)
                 (from             ((C default-language)   file))
                 (to               'bytecode)
                 (env              ((C default-environment) from))
                 (opts             '())
                 (canonicalization 'relative))
    (with-fluids (((C %in-compile                     )   #t               )
		  ((C %in-file                        )   file             )
                  (%dont-warn-list                        '()              )
                  ((C %file-port-name-canonicalization)   canonicalization )
                  ((C %current-file%                  )   file))
      
      (let* ((comp (or output-file ((C compiled-file-name) file)
                       (error "failed to create path for auto-compiled file"
                              file)))
             (in  ((C open-input-file) file))
             (enc ((C file-encoding)   in)))
        ;; Choose the input encoding deterministically.
        ((C set-port-encoding!) in (or enc "UTF-8"))
        
        ((C ensure-directory) ((C dirname) comp))
        ((C call-with-output-file/atomic) comp
         (lambda (port)
           (((C language-printer) ((C ensure-language) to))
            ((C read-and-compile)
             in #:env env #:from from #:to to #:opts
             (cons* #:to-file? #t opts))
            port))
         file)
        comp))))

;; MESSAGE (Mute some variable warnings)

(define w-types
    (map (lambda (args)
         (apply (M make-warning-type) args))

       (let-syntax ((emit
                     (lambda (s)
                       (syntax-case s ()
                         ((_ port fmt args ...)
                          (string? (syntax->datum #'fmt))
                          (with-syntax ((fmt
                                         (string-append "~a"
                                                        (syntax->datum
                                                         #'fmt))))
                            #'(format port fmt
                                      (fluid-ref (M *current-warning-prefix*))
                                      args ...)))))))
         `((unsupported-warning ;; a "meta warning"
            "warn about unknown warning types"
            ,(lambda (port unused name)
               (emit port "warning: unknown warning type `~A'~%"
                name)))

           (unused-variable
            "report unused variables"
            ,(lambda (port loc name)
               (emit port "~A: warning: unused variable `~A'~%"
                loc name)))

           (unused-toplevel
            "report unused local top-level variables"
            ,(lambda (port loc name)
               (emit port
                "~A: warning: possibly unused local top-level variable `~A'~%"
                loc name)))

           (unbound-variable
            "report possibly unbound variables"
            ,(lambda (port loc name)
               (if (and (not (member name
                                     (fluid-ref
                                      %dont-warn-list)))
                        (not (member name
                                     (hash-ref %eval-no-warn
                                               (current-module) '()))))
                   (emit port
                    "~A: warning: possibly unbound variable `~A'~%"
                    loc name))))

           (macro-use-before-definition
            "report possibly mis-use of macros before they are defined"
            ,(lambda (port loc name)
               (emit port
                "~A: warning: macro `~A' used before definition~%"
                loc name)))

           (arity-mismatch
            "report procedure arity mismatches (wrong number of arguments)"
            ,(lambda (port loc name certain?)
               (if certain?
                   (emit port
                    "~A: warning: wrong number of arguments to `~A'~%"
                    loc name)
                   (emit port
                    "~A: warning: possibly wrong number of arguments to `~A'~%"
                    loc name))))

           (shadowed-toplevel
            ""
            ,(lambda x x))
           
           (duplicate-case-datum
            "report a duplicate datum in a case expression"
            ,(lambda (port loc datum clause case-expr)
               (emit port
                "~A: warning: duplicate datum ~S in clause ~S of case expression ~S~%"
                loc datum clause case-expr)))

           (bad-case-datum
            "report a case datum that cannot be meaningfully compared using `eqv?'"
            ,(lambda (port loc datum clause case-expr)
               (emit port
                "~A: warning: datum ~S cannot be meaningfully compared using `eqv?' in clause ~S of case expression ~S~%"
                loc datum clause case-expr)))

           (format
            "report wrong number of arguments to `format'"
            ,(lambda (port loc . rest)
               (define (escape-newlines str)
                 (list->string
                  (string-fold-right (lambda (c r)
                                       (if (eq? c #\newline)
                                           (append '(#\\ #\n) r)
                                           (cons c r)))
                                     '()
                                     str)))

               (define (range min max)
                 (cond ((eq? min 'any)
                        (if (eq? max 'any)
                            "any number" ;; can't happen
                            (emit #f "up to ~a" max)))
                       ((eq? max 'any)
                        (emit #f "at least ~a" min))
                       ((= min max) (number->string min))
                       (else
                        (emit #f "~a to ~a" min max))))

               ((M match) rest
                 (('simple-format fmt opt)
                  (emit port
                   "~A: warning: ~S: unsupported format option ~~~A, use (ice-9 format) instead~%"
                   loc (escape-newlines fmt) opt))
                 (('wrong-format-arg-count fmt min max actual)
                  (emit port
                   "~A: warning: ~S: wrong number of `format' arguments: expected ~A, got ~A~%"
                   loc (escape-newlines fmt)
                   (range min max) actual))
                 (('syntax-error 'unterminated-iteration fmt)
                  (emit port "~A: warning: ~S: unterminated iteration~%"
                   loc (escape-newlines fmt)))
                 (('syntax-error 'unterminated-conditional fmt)
                  (emit port "~A: warning: ~S: unterminated conditional~%"
                   loc (escape-newlines fmt)))
                 (('syntax-error 'unexpected-semicolon fmt)
                  (emit port "~A: warning: ~S: unexpected `~~;'~%"
                   loc (escape-newlines fmt)))
                 (('syntax-error 'unexpected-conditional-termination fmt)
                  (emit port "~A: warning: ~S: unexpected `~~]'~%"
                   loc (escape-newlines fmt)))
                 (('wrong-port wrong-port)
                  (emit port
                   "~A: warning: ~S: wrong port argument~%"
                   loc wrong-port))
                 (('wrong-format-string fmt)
                  (emit port
                   "~A: warning: ~S: wrong format string~%"
                        loc fmt))
                 (('non-literal-format-string)
                  (emit port
                   "~A: warning: non-literal format string~%"
                   loc))
                 (('wrong-num-args count)
                  (emit port
                   "~A: warning: wrong number of arguments to `format'~%"
                   loc))
                 (else
                  (emit port "~A: `format' warning~%" loc)))))))))

(define (lookup-warning-type name)
  "Return the warning type NAME or `#f' if not found."
  (find (lambda (wt)
          (eq? name ((M warning-type-name) wt)))
        (M %warning-types)))

(define (warning type location . args)
  "Emit a warning of type TYPE for source location LOCATION (a source
property alist) using the data in ARGS."
  (let ((wt   ((M lookup-warning-type) type))
        (port ((M current-warning-port))))
    (if ((M warning-type?) wt)
        (apply ((M warning-type-printer) wt)
               port ((M location-string) location)
               args)
        ((M format) port "~A: unknown warning type `~A': ~A~%"
                ((M location-string) location) type args))))

(define (find p l)
  (if (pair? l)
      (let ((x (car l)))
        (if (p x)
            x
            (find p (cdr l))))
      #f))

(cond-expand
  (guile-3.0
   (define-set-M lookup-warning-type lookup-warning-type)
   (define-set-M warning             warning)
   (set! %warning-types w-types))

  ((or guile-2.0 guile-2.2)
   (define-M %warning-types w-types)
   ;; List of known warning types.

   (define-exp-M lookup-warning-type
     (lambda (name)
       "Return the warning type NAME or `#f' if not found."
       (find
        (lambda (wt)
          (eq? name ((M warning-type-name) wt)))
        (M %warning-types))))
   ))

(define guile-load  (@@ (guile) primitive-load-path))
(define guile-load2 (@@ (guile) primitive-load))

(cond-expand
  (guile-3.0
   (define (more-recent? stat1 stat2)
     ;; Return #t when STAT1 has an mtime greater than that of STAT2.
     (or (> (stat:mtime stat1) (stat:mtime stat2))
         (and (= (stat:mtime stat1) (stat:mtime stat2))
              (>= (stat:mtimensec stat1)
                  (stat:mtimensec stat2)))))

   (define (canonical->suffix canon)
     (cond
      ((and (not (string-null? canon))
            ((@@ (guile) file-name-separator?) (string-ref canon 0)))
       canon)
      ((and (eq? ((@@ (guile) system-file-name-convention)) 'windows)
            ((@@ (guile) absolute-file-name?) canon))
       ;; An absolute file name that doesn't start with a separator
       ;; starts with a drive component.  Transform the drive component
       ;; to a file name element:  c:\foo -> \c\foo.
       (string-append (@@ (guile) file-name-separator-string)
                      (substring canon 0 1)
                      (substring canon 2)))
      (else canon)))
   
   (define compiled-extension
     ;; File name extension of compiled files.
     (cond ((or (null? (@@ (guile) %load-compiled-extensions))
                (string-null? (car (@@ (guile) %load-compiled-extensions))))
            (warn "invalid %load-compiled-extensions"
                  (@@ (guile) %load-compiled-extensions))
            ".go")
           (else (car (@@ (guile) %load-compiled-extensions)))))

   (define (fallback-file-name canon-file-name)
     ;; Return the in-cache compiled file name for source file
     ;; CANON-FILE-NAME.
     
     ;; FIXME: would probably be better just to append
     ;; SHA1(canon-file-name) to the %compile-fallback-path, to avoid
     ;; deep directory stats.
     (and (@@ (guile) %compile-fallback-path)
          (string-append (@@ (guile) %compile-fallback-path)
                         (canonical->suffix canon-file-name)
                         compiled-extension)))

   (define (get-go abs-file-name)
     (and=> ((@@ (guile) false-if-exception)
             ((@@ (guile) canonicalize-path) abs-file-name))
            (lambda (canon)
              (and=> (fallback-file-name canon)
                     (lambda (go-file-name) go-file-name)))))
  
   (define (docompile? name)
     (let* ((scmstat (catch #t (lambda () (stat name #f)) (lambda x #f)))
            (go      (get-go name))
            (gostat  (catch #t (lambda () (stat go #f)) (lambda x #f))))
       (if scmstat
           (if (not
                (and gostat scmstat (more-recent? gostat scmstat)))
               go
               #f)
           #f)))

   (define pload 
     (lambda (p . q)
       #;(define (goit x)
         (string-join
          (let* ((r   (string-split x #\/))
                 (rev (reverse r))
                 (c   (string-append (car (string-split (car rev) #\.))
                                     ".go")))
            (reverse (cons c (cdr rev))))
          "/"))
       (define (goit x) x)
       (let ((tag (make-prompt-tag)))
         (call-with-prompt
          tag
          (lambda ()
            (let ((q (list (lambda x (abort-to-prompt tag)))))
              (let lp ((l *extension-dispatches*))
                (if (pair? l)
                    (let lp2 ((u (caar l)))
                      (if (pair? u)
                          (aif it (%search-load-path
                                   (string-append p "." (car u)))
                               (begin
                                 (aif go (aif it (docompile? it) (goit it) #f)
                                      (begin
                                        (pk "Compile File " it "to .go" go)
                                        ((@ (system base compile) compile-file)
                                         it #:output-file go)))
                                 (apply guile-load (get-go it) q))
                               (lp2 (cdr u)))
                          (lp (cdr l))))
                    (apply guile-load p q)))))
          (lambda (k)
            (let lp ((l *extension-dispatches*))
              (if (pair? l)
                  (let lp2 ((u (caar l)))
                    (if (pair? u)
                        (aif it (%search-load-path
                                 (string-append p "." (car u)))
                             (let ((go (goit (get-go it))))
                               (pk "Compile File " it "to go" go)
                               ((@ (system base compile) compile-file)
                                it #:output-file go)
                               (apply guile-load go q))
                             (lp2 (cdr u)))
                        (lp (cdr l))))                 
                  (if (pair? q)
                      ((car q))
                      (error (string-append "no code for path " p)))))))))))

  (guile-2.0
   (define pload 
     (lambda (p . q)
       (let ((tag (make-prompt-tag)))
         (call-with-prompt
          tag
          (lambda ()
            (apply guile-load p q))
          (lambda (k)
            (let lp ((l *extension-dispatches*))
              (if (pair? l)
                  (let lp2 ((u (caar l)))
                    (if (pair? u)
                        (aif it (%search-load-path
                                 (string-append p "." (car u)))
                             (apply guile-load it q)
                             (lp2 (cdr u)))
                        (lp (cdr l))))                 
                  (if (pair? q)
                      ((car q))
                      (error (string-append "no code for path " p))))))))))))

 
(define-set-G primitive-load-path pload)

(define-syntax-rule (C2 m) (@@ (system vm program) m))

(cond-expand  
  (guile-3.0
   (define mk-source
     (case-lambda
       ((x n)
        (catch #t
          (lambda ()
            (cons* n
                   (cdr (assoc 'filename x))
                   (- (cdr (assoc 'line x)) 1)
                   (cdr (assoc 'column x))))
          (lambda x 
            #f)))
       ((x) (mk-source x 0))))

   (define (source-for source)
     (catch #t
       (lambda ()
         (cons* 0
                ((C2 source-file) source)
                ((C2 source-line) source)
                ((C2 source-column) source)))
       (lambda x #f)))

   (define* (program-source proc ip #:optional
                            (sources ((C2 program-sources) proc)))
     (define (get n) (mk-source (procedure-properties- proc) n))
     (let ((x
            (let lp ((source #f) (sources sources))
              ((C2 match) sources
                (() source)
                (((and s (pc . _)) . sources)
                 (if (<= pc ip)
                     (lp s sources)
                     source))))))
       (if x
           (aif it (get (car x))
                it
                x)
           (aif it (get 0)
                it
                x))))

                        
   (define* (print-program #:optional program (port (current-output-port))
                           #:key
                           (addr                 ((C2 program-code) program))
                           (always-print-addr?   #f)
                           (never-print-addr?    #f)
                           (always-print-source? #f)
                           (never-print-source?  #f)
                           (name-only?           #f)
                           (print-formals?       #t))
     (let* ((pdi ((C2 find-program-debug-info) addr))
            ;; It could be the procedure had its name property set via the
            ;; procedure property interface.
            (name (or (and program (procedure-name program))
                      (and pdi ((C2 program-debug-info-name) pdi))))
            (source (or (and program (mk-source
                                      (procedure-properties- program)))
                        (source-for
                         ((C2 match) ((C2 find-program-sources) addr)
                          (() #f)
                          ((source . _) source)))))
            (formals (if program
                         ((C2 program-arguments-alists) program)
                         (let ((arities ((C2 find-program-arities) addr)))
                           (if arities
                               (map (C2 arity-arguments-alist) arities)
                               '())))))
       (define (hex n)
         (number->string n 16))

       (cond
        ((and name-only? name)
         (format port "~a" name))
        (else
         (format port "#<procedure")
         
         (format port " ~a"
                 (or name
                     (and program (hex (object-address program)))
                     (if never-print-addr?
                         ""
                         (string-append "@" (hex addr)))))
         
         (when (and always-print-addr? (not never-print-addr?))
           (unless (and (not name) (not program))
             (format port " @~a" (hex addr))))
         
         (when (and source (not never-print-source?)
                    (or always-print-source? (not name)))
           (format port " at ~a:~a:~a"
                   (or ((C2 source:file) source) "<unknown port>")
                   ((C2 source:line-for-user) source)
                   ((C2 source:column) source)))
         
         (unless (or (null? formals) (not print-formals?))
           (format port "~a"
                   (string-append
                    " " (string-join
                         (map (lambda (a)
                                ((C2 object->string)
                                 ((C2 arguments-alist->lambda-list) a)))
                              formals)
                         " | "))))
         (format port ">")))))

   (define (write-program prog port)
     (print-program prog port))

   (define-set-C2 print-program  print-program)
   (define-set-C2 write-program  write-program)
   (define-set-C2 program-source program-source))
  (guile-2.0
   (values)))

(define-syntax-rule (R m) (@@ (system repl debug) m))

(cond-expand
  (guile-2.2
   (define (frame-name frame)
     (aif it (procedure-properties-- frame #f)
          (aif it (assoc it 'name)
               it
               #f)
          #f))
   
   (define (frame-source frame x)
     (define (get n) (mk-source (procedure-properties-- frame '()) n))
       (if x
           (aif it (get (car x))
                it
                x)
           (aif it (get 0)
                it
                x)))

   (define* (print-frame frame #:optional (port (current-output-port))
                         #:key index (width ((R terminal-width))) (full? #f)
                         (last-source #f) next-source?)
     (define (source:pretty-file source)
       (if source
           (or ((R source:file) source) "current input")
           "unknown file"))
     (let* ((source (frame-source frame ((R frame-source) frame)))
            (file (source:pretty-file source))
            (line (and=> source (R source:line-for-user)))
            (col (and=> source (R source:column))))
       (if (and file (not (equal? file (source:pretty-file last-source))))
           (format port "~&In ~a:~&" file))
       (format port "~9@a~:[~*~3_~;~3d~] ~v:@y~%"
               (if line (format #f "~a:~a" line col) "")
               index index width
               ((R frame-call-representation) frame #:top-frame? (zero? index)))
       (if full?
           ((R print-locals) frame #:width width
                         #:per-line-prefix "     "))))

   (define* (print-frames frames
                          #:optional (port (current-output-port))
                          #:key (width ((R terminal-width))) (full? #f)
                          (forward? #f) count)
     (let* ((len (vector-length frames))
            (lower-idx (if (or (not count) (positive? count))
                           0
                           (max 0 (+ len count))))
            (upper-idx (if (and count (negative? count))
                           (1- len)
                           (1- (if count (min count len) len))))
            (inc (if forward? 1 -1)))
       (let lp ((i (if forward? lower-idx upper-idx))
                (last-source #f))
         (if (<= lower-idx i upper-idx)
             (let* ((frame (vector-ref frames i)))
               (print-frame frame port #:index i #:width width #:full? full?
                            #:last-source last-source)
               (lp (+ i inc)
                   (frame-source frame ((R frame-source) frame))))))))

   (define-set-R print-frame   print-frame)
   (define-set-R print-frames  print-frames))

  (guile-2.0
   (values)))
