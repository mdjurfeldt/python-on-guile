(define-module (language python module builtins)
  #:use-module (language python module python))

(cond-expand
  (guile-3.0
   (define (re-export! mod l)
     (module-re-export! mod l #:replace? #t)))
  ((or guile-2.0  guile-2.2)
   (define (re-export! mod l)
     (module-re-export! mod l))))

(define-syntax re-export-all
  (syntax-rules ()
    [(_ iface)
     (module-for-each 
      (lambda (name . l)
        (re-export! (current-module) ((@ (guile) list) name)))
      (resolve-interface 'iface))]
    [(_ iface _ li)
     (let ((l 'li))
       (module-for-each 
        (lambda (name . l)        
          (if (not (member name l))
              (re-export! (current-module) ((@ (guile) list) name))))
        (resolve-interface 'iface)))]))

(re-export-all (language python module python))
  
