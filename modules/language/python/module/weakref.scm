(define-module (language python module weakref)
  #:use-module (language python dict)
  #:use-module (language python set)
  #:export (WeakKeyDictionary WeakValueDictionary WeakSet))

(define WeakKeyDictionary   weak-key-dict)
(define WeakValueDictionary weak-value-dict)
(define WeakSet             weak-set)
  

  
