(define-module (language python module sys)
  #:use-module (rnrs bytevectors)
  #:use-module (language python exceptions)
  #:use-module (language python hash)
  #:use-module (language python compile)
  #:use-module (language python list)
  #:use-module (language python module io)
  #:use-module (language python try)
  #:use-module (language python string)
  #:use-module (language python module python)
  #:use-module (oop pf-objects)
  #:export (argv byteorder copyright implementation
		     stdin     stdout     stderr
		   __stdin__ __stdout__ __stderr__
		   exit version_info version api_version
                   warnoptions winver _xoption 
                   tarcebacklimit platform maxsize hash_info
                   base_prefix path))

(define-syntax stdin_
  (lambda (x)
    (syntax-case x (set!)
      ((set! stdin port)
       #'(set-current-input-port port))
      ((_ . _)
       (error "sys.stdin is not a function"))
      (s #'(current-input-port)))))

(define-syntax stdout_
  (lambda (x)
    (syntax-case x (set!)
      ((set! stdin port)
       #'(set-current-output-port port))
      ((_ . _)
       (error "sys.stdin is not a function"))
      (s #'(current-output-port)))))

(define-syntax stderr_
  (lambda (x)
    (syntax-case x (set!)
      ((set! stdin port)
       #'(set-current-error-port port))
      ((_ . _)
       (error "sys.stdin is not a function"))
      (s #'(current-error-port)))))

(define stdin  (FileIO stdin_ ))
(define stderr (FileIO stderr_))
(define stdout (FileIO stdout_))

(define __stdin__  stdin)
(define __stdout__ stdout)
(define __stderr__ stderr)

(define abiflags         '(""))
(define argv              (command-line))
(define base_exec_prefix  #f)
(define base_prefix       "/usr")
(define byteorder         (if (eq? (native-endianness) 'little)
			      "little"
			      "big"))
(define builtin_module_names '())
(define call_tracing
  (lambda (func args)
    (apply func args)))
(define copyright "Stefan Israelsson Tampe")
(define _clear_type_cache
  (lambda () (values)))
(define _current_frames
  (lambda () '()))
(define _debugmallocstats
  (lambda () (values)))

(define dllhandle 0)

(define _ #f)

(define displayhook
  (lambda (value)
    (when (not (eq? value None))
      (write stdout (repr value))
      (set! _ value))))

(define dont_write_bytecode #f)
(define excepthook
  (lambda (type value traceback)
    (write stderr (repr (list type value traceback)))))

(define __displayhook__ displayhook)
(define __excepthook__  excepthook)

(define exc_info
  (lambda () (values)))

(define exec_prefix "/usr/local")
(define executable "")
(define exit
  (lambda (arg)
    (if (fluid-ref exit-fluid)
        (abort-to-prompt exit-prompt arg)
        ((@ (guile) exit) arg))))

(define flags '())
(define float_info '())
(define float_repr_style #f)

(define getallocatedblocks
  (lambda () 0))
(define getcheckinterval
  (lambda () 0))

(define getdefaultencoding
  (lambda () #f))

(define getdlopenflags
  (lambda () '()))

(define getfilesystemencoding
  (lambda () #f))

(define getfilesystemcodeerrors
  (lambda () #f))

(define getrefcount
  (lambda () 0))

(define getsizeof
  (lambda x #f))

(define getswitchinterval
  (lambda () 0))

(define _getframe
  (lambda x 0))

(define getwindowsversion
  (lambda () 0))

(define implementation "guile")

(define int_info #f)

(define __interactivehook__ (lambda () (values)))


(define intern (lambda (s) (values)))

(define is_finalizing
  (lambda () #f))

(define last_type      #f)
(define last_value     #f)
(define last_traceback #f)
    
(define maxsize     (- (ash 1 63) 2))
(define maxunicode #f)

(define meta_path (to-pylist '()))
(define modules (@ (language python module) modules))
(define path (to-pylist %load-path))
(define path_hooks (to-pylist '()))
(define path_importer_cache (make-hash-table))

(define-python-class hash_info ()
  (define modulus pyhash-N)
  (define inf     ((@ (guile) hash) 'inf pyhash-N))
  (define nan     ((@ (guile) hash) 'nan pyhash-N)))

(define platform "linux")

(define prefix "")
(define ps1    "")
(define ps2    "")

(define setcheckinterval
  (lambda (i) (values)))

(define setdlopenflags
  (lambda (n) (values)))

(define setprofile
  (lambda (f) (values)))

(define setrecursionlimit
  (lambda (n) (values)))

(define setswitchinterval
  (lambda (i) (values)))

(define settrace
  (lambda (f) (values)))

(define set_asyncgen_hooks
  (lambda (a b) (values)))

(define set_coroutine_wrappr
  (lambda (w) (values)))

(define _enablelegacywindowsfsencoding
  (lambda () (values)))

(define thread_info #f)

(define tracebacklimit 0)

(define version     "0.0.0")
(define api_version "0.0.0")
(define version_info '(3 7))
(define warnoptions   #f)
(define winver        0)
(define _xoptions    (make-hash-table))
















    
  

    

