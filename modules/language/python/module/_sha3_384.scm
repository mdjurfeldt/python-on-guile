(define-module (language python module _sha3_384)
  #:use-module (language python checksum)
  #:use-module (oop pf-objects)
  #:export (sha3_384))

(define-python-class sha3_384 (Summer)
  (define name     "sha3_384")
  (define digest_size 48)
  
  (define _command "/usr/bin/sha3_384sum"))
