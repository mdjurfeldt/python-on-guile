(define-module (language python module signal)
  #:use-module (language python module enum)
  #:export (SIGTERM SIGKILL))

(define SIGTERM 15)
(define SIGKILL 9)
