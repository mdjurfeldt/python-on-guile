(define-module (language python module python )
  #:use-module (language python module _python)
  #:use-module (language python compile       )
  #:use-module (language python module        )
  #:use-module ((language python module string) #:select ())
  #:use-module (language python memoryview    )
  #:use-module ((oop pf-objects) #:select (define-python-class ref))
  #:use-module ((language python format2) #:select ())
  #:re-export (memoryview)
  #:replace (close)
  #:export (ClassMethod StaticMethod Funcobj *main* __name__))

(cond-expand
  (guile-3.0
   (define (re-export! mod l)
     (module-re-export! mod l #:replace? #t)))

  ((or guile-2.0  guile-2.2)
   (define (re-export! mod l)
     (module-re-export! mod l))))
  

(define-syntax re-export-all
  (syntax-rules ()
    [(_ iface)
     (module-for-each 
      (lambda (name . l)
        (re-export! (current-module)
                    ((@ (guile) list) name)))
      (module-public-interface (resolve-module 'iface)))]
    [(_ iface _ li)
     (let ((l 'li))
       (module-for-each 
        (lambda (name . l)
          (if (not (member name l))
              (re-export! (current-module)
                          ((@ (guile) list) name))))
        
        (module-public-interface (resolve-module 'iface))))]))

(set! (@@ (oop dict) splitm)
  (@@ (language python module re) splitm))

(set! (@@ (oop dict) splitmm)
  (@@ (language python module re) splitmm))

(set! (@ (language python module os) path)
  (Module '(path os module python language) '(path os)))

(define (close fd)
  (cond
   ((integer? fd)
    (close-fdes fd))
   ((port? fd)
    (close fd))
   (else
    ((ref fd 'close)))))
      

(re-export-all (language python module _python))

(set! (@@ (language python eval) MM) (@@ (language python compile) void))

(define-python-class ClassMethod  ())
(define-python-class StaticMethod ())
(define-python-class Funcobj      ())
(define *main* #f)
(define __name__ "python")
