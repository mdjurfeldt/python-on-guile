(define-module (language python module _sha256)
  #:use-module (language python checksum)
  #:use-module (oop pf-objects)
  #:export (sha256))

(define-python-class sha256 (Summer)
  (define name     "sha256")
  (define digest_size 32)
  
  (define _command "/usr/bin/sha256sum"))
