(define-module (language python module _blake2b)
  #:use-module (language python checksum)
  #:use-module (oop pf-objects)
  #:export (blake2b))

(define-python-class blake2b (Summer)
  (define name     "blake2b")
  (define digest_size 64)
  
  (define _command "/usr/bin/blake2bsum"))
