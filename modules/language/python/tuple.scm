(define-module (language python tuple)
  #:use-module (oop goops)
  #:use-module (oop pf-objects)
  #:use-module (language python hash)
  #:use-module (language python for)
  #:use-module (language python bool)
  #:use-module (language python persist)
  #:export (tuple <py-tuple> defpair))


(define-class <py-tuple> () l)
(name-object <py-tuple>)
(cpit <py-tuple>
      (o (lambda (o l)
	   (slot-set! o 'l (map (lambda (x) x) l)))
	 (list
	  (slot-ref o 'l))))

(define-method (py-hash (o <py-tuple>)) (py-hash (slot-ref o 'l)))
(define-method (py-class (o <py-tuple>) tuple))
(define-method (py-equal? (o1 <py-tuple>) o2) (equal? (slot-ref o1 'l) o2))
(define-method (py-equal? o1 (o2 <py-tuple>)) (equal? o1 (slot-ref o2 'l)))
(define-method (bool (o <py-tuple>)) (pair? (slot-ref o 'l)))
(define-method (wrap-in (o <py-tuple>))
  (wrap-in (slot-ref o 'l)))

(define-python-class tuple (<py-tuple>)
  (define __init__
    (case-lambda
      ((self)
       (slot-set! self 'l '()))
      ((self it)
       (slot-set! self 'l
                  (for ((x : it)) ((l '()))
                       (cons x l)
                       #:final
                       (reverse l))))))
  (define __repr__
    (lambda (self) (format #f "~a" (slot-ref self 'l)))))

(name-object tuple)

(define-syntax-rule (defpair (f o . u) code ...)
  (begin
    (define-method (f (o <pair>)     . u)
      code ...)
    (define-method (f (o <py-tuple>) . l)
      (let ((o (slot-ref o 'l)))
        (apply f o l)))))
